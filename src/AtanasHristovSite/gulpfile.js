﻿/// <binding BeforeBuild='site-build' AfterBuild='site-dist' />
var gulp = require('gulp');
var gulpSass = require('gulp-sass');
var concat = require('gulp-concat');
var order = require('gulp-order');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');


gulp.task('3rdparty-css', function () {
    gulp
        .src([
            './bower_components/bootstrap/scss/bootstrap.scss',
            './bower_components/tether/dist/css/tether.css',
            './bower_components/font-awesome/css/font-awesome.css',
            './wwwroot/src/sass/3rdparty/bootstrap-custom.scss'
        ])
        .pipe(gulpSass())
        .pipe(gulp.dest('./wwwroot/build/css/3rdparty/bootstrap'));
    gulp
        .src([
            './wwwroot/build/css/3rdparty/bootstrap/bootstrap.css',
            './wwwroot/build/css/3rdparty/bootstrap/tether.css',
            './wwwroot/build/css/3rdparty/bootstrap/font-awesome.css',
            './wwwroot/build/css/3rdparty/bootstrap/bootstrap-custom.css',
        ])
        .pipe(concat('3rdparty.css'))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./wwwroot/dist/css'));
    gulp
        .src([
            './bower_components/font-awesome/fonts/*',
        ])
        .pipe(gulp.dest('./wwwroot/build/css/3rdparty/fonts'))
        .pipe(gulp.dest('./wwwroot/dist/fonts'));
});

gulp.task('3rdparty-js', function () {
    gulp
        .src([
            './bower_components/jquery/dist/jquery.js',
            './bower_components/tether/dist/js/tether.js',
            './bower_components/bootstrap/dist/js/bootstrap.min.js',
            // './wwwroot/src/js/3rdparty/jquery.scrollzer.min.js',
            './bower_components/jquery-scrolly/jquery.scrolly.js',
            './bower_components/skel/dist/skel.min.js',
        ])
        .pipe(order([
            'bower_components/jquery/dist/jquery.js',
            'bower_components/tether/dist/js/tether.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            // 'wwwroot/src/js/3rdparty/jquery.scrollzer.min.js',
            'bower_components/jquery-scrolly/jquery.scrolly.js',
            'bower_components/skel/dist/skel.min.js',
        ], { base: './' }))
        .pipe(concat('3rdparty.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./wwwroot/dist/js'));
});


gulp.task('site-build', function () {

    gulp
        .src([
            './wwwroot/src/sass/site/*.scss'
        ])
        .pipe(gulpSass())
        .pipe(gulp.dest('./wwwroot/build/css/site'));

    gulp
        .src([
            './wwwroot/src/js/site/*.js'
        ])
        .pipe(gulp.dest('./wwwroot/build/js/site'));

});


gulp.task('site-dist', function () {

    gulp
        .src([
            './wwwroot/build/css/site/main.css'
        ])
        .pipe(order([
            'wwwroot/build/css/site/main.css'
        ], { base: './' }))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(concat('site.css'))
        .pipe(gulp.dest('./wwwroot/dist/css'));

    gulp
        .src([
            './wwwroot/build/js/site/jquery.scrollzer.js',
            './wwwroot/build/js/site/util.js',
            './wwwroot/build/js/site/main.js'
        ])
        .pipe(order([
            'wwwroot/build/js/site/jquery.scrollzer.js',
            'wwwroot/build/js/site/util.js',
            'wwwroot/build/js/site/main.js'
        ], { base: './' }))
        .pipe(concat('site.js'))
        .pipe(gulp.dest('./wwwroot/dist/js'));

});

