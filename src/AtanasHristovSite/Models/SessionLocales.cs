﻿namespace AtanasHristovSite.Models
{
    public class SessionLocales
    {
        public string CultureCode { get; set; } = "en-US";
        public string CultureLang { get; set; } = "en";
    }
}
