using AtanasHristovSite.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace AtanasHristovSite
{
    public class Startup
    {
        private readonly IConfigurationRoot _configuration;
        private readonly IHostingEnvironment _environment;


        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets();
            }

            _configuration = builder.Build();
            _environment = env;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<SessionLocales>(_configuration.GetSection("SessionLocales"));
            //services.Configure<RazorViewEngineOptions>(options =>
            //{
            //    var expander = new CustomViewLocationExpander(_configuration["ViewsLocation"]);
            //    options.ViewLocationExpanders.Add(expander);
            //});

            //services.AddAntiforgery(options =>
            //{
            //    options.CookieName = "antiforgerytoken";
            //    options.HeaderName = "x-antiforgery-token";
            //    options.RequireSsl = _environment.IsProduction();
            //});

            services.AddMvc(options =>
            {
                if (_environment.IsProduction())
                {
                    // options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                    // options.Filters.Add(new RequireHttpsAttribute());
                }
            });

            services.AddSingleton<IConfiguration>(_configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(_configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //if (env.IsDevelopment())
            //{
                app.UseDeveloperExceptionPage()
                   .UseDatabaseErrorPage()
                   .UseBrowserLink();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error")
            //       .UseStatusCodePages();
            //}

            app.UseStaticFiles();
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync($"Hello World! Now is : {DateTime.Now.ToString():F}");
            //});

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
