﻿using Microsoft.AspNetCore.Mvc.Razor;
using System.Collections.Generic;
using System.Linq;

public class CustomViewLocationExpander : IViewLocationExpander
{
    private readonly string _viewsLocation;

    public CustomViewLocationExpander(string viewsLocation)
    {
        _viewsLocation = viewsLocation;
    }

    public virtual IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
    {
        return viewLocations.Select(s => 
        {
            return string.IsNullOrWhiteSpace(_viewsLocation)
                ? s
                : s.Replace("/Views/", $"{_viewsLocation}/Views/");
        });
    }

    public void PopulateValues(ViewLocationExpanderContext context)
    {
    }
}