﻿using AtanasHristovSite.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace AtanasHristovSite.Controllers
{
    public class HomeController : Controller
    {
        private readonly SessionLocales _sessionLocales;
        public HomeController(IOptions<SessionLocales> sessionLocales)
        {
            _sessionLocales = sessionLocales.Value;
        }

        public IActionResult Index()
        {
            ViewData["CultureLang"] = _sessionLocales.CultureLang;

            return View();
        }
    }
}
